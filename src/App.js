import React, { Component } from "react";

import "./App.css";
import Pagination from "./Components/Pagination/Pagination";
import HeaderComponent from "./Components/HeaderComponent/HeaderComponent";
import BodyComponent from "./Components/BodyComponent/BodyComponent";
import { BrowserRouter as Router } from "react-router-dom";
// import { Route } from "react-router-dom/Route";
import { NavLink } from "react-router-dom";
import PagePagination from "./Components/Pagination/PagePagination";
import DisplayIssueComponent from "./Components/DisplayIssueComponent/DisplayIssueComponent";
let Route = require("react-router-dom").Route;

const particularIssue = match => {
  return (
    <div>
      <HeaderComponent />
      <DisplayIssueComponent issueId={match.match.params.issueId} />
    </div>
  );
};
const home = match => {
  return (
    <div className="App">
      {/* <HeaderComponent />
      <BodyComponent /> */}

      <Pagination data={match} />
    </div>
  );
};
const pagepagination = match => {
  return (
    <div>
      <PagePagination data={match} />
    </div>
  );
};
class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={home} />
        <Route path="/issue/:issueId" exact component={particularIssue} />
        <Route path="/page/:pageNo" exact component={pagepagination} />
      </Router>
    );
  }
}

export default App;

import React from "react";
import firebase from "firebase";
var provider = new firebase.auth.GithubAuthProvider();
var config = {
  apiKey: "AIzaSyDUySN3iuQVHR6xIQAVgkIwARAt4piE8O8",
  authDomain: "enduring-stage-229515.firebaseapp.com",
  databaseURL: "https://enduring-stage-229515.firebaseio.com",
  projectId: "enduring-stage-229515",
  storageBucket: "enduring-stage-229515.appspot.com",
  messagingSenderId: "784358906988"
};
firebase.initializeApp(config);
provider.addScope("repo");

function auth() {
  return firebase.auth().signInWithPopup(provider);
}
export default auth;

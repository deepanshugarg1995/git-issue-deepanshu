import React from "react";
import { withRouter } from "react-router-dom";
const but = withRouter(({ history }) => {
  return (
    <button
      type="button"
      onClick={() => {
        history.push("/new-location");
      }}
    >
      Click Me!
    </button>
  );
});
export default but;

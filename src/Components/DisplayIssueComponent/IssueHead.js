import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./DisplayIssueComponent.css";
import axios from "axios";
function IssueHead(props) {
  return (
    <div>
      <span className="issue-topic">{props.data}</span> &nbsp; &nbsp;
      <span className="issue-topic-id">#{props.id}</span>
    </div>
  );
}
export default IssueHead;

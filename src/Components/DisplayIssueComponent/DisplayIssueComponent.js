import auth from "../../auth";
import MaterialIcon, { colorPalette } from "material-icons-react";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./DisplayIssueComponent.css";
import axios from "axios";
import IssueHead from "./IssueHead";
import moment from "moment";
import $ from "jquery";
import but from "./but";
import { withRouter, Redirect } from "react-router-dom";
import { addUser } from "../action/addUser";
import { connect } from "react-redux";
// this also works with react-router-native

const ReactMarkdown = require("react-markdown");

class DisplayIssueComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      originalIssue: "",
      originalCommments: "",
      originalIssueState: 0,
      originalCommmentsState: 0,
      filterByAuthor: "",
      sortBy: "",
      login: null
    };
    // this.componentDidMount = this.componentDidMount.bind(this);
  }
  getData() {
    let issueId = this.props.issueId;
    let self = this;

    let comUrl =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/" +
      issueId +
      "/comments";
    axios
      .get(comUrl)
      .then(function(response) {
        if (response.data.length == 0) {
          self.setState({
            originalCommments: response,
            originalCommmentsState: 0
          });
        } else {
          self.setState({
            originalCommments: response,
            originalCommmentsState: 1
          });
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  deleteComment(value) {
    let self = this;
    // let newData = this.state.originalCommments.data.filter((val, index) => {
    //   if (val["id"] != value) {
    //     return val;
    //   }
    // });
    // let stateData = this.state.originalCommments;
    // stateData["data"] = newData;
    // if (newData.length == 0) {
    //   this.setState({
    //     originalCommments: stateData,
    //     originalCommmentsState: 0
    //   });
    // } else {
    //   this.setState({
    //     originalCommments: stateData
    //   });
    // }
    $.ajax({
      url:
        "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/comments/" +
        value +
        "?access_token=" +
        this.state.login,
      type: "DELETE",
      success: function() {
        self.getData();
      },
      error: function(xhr, status, error) {
        console.log(xhr.responseText);
      }
    });
  }

  componentDidMount() {
    let issueId = this.props.issueId;
    let self = this;

    let url =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/" +
      issueId;

    axios
      .get(url)
      .then(function(response) {
        self.setState({
          originalIssue: response.data,
          originalIssueState: 1
        });
      })
      .catch(function(error) {
        // this.setState({
        //   originalIssue: "data error"
        // });
        console.log(error);
      });
    let comUrl =
      "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/" +
      issueId +
      "/comments";

    axios
      .get(comUrl)
      .then(function(response) {
        self.setState({
          originalCommments: response,
          originalCommmentsState: 1
        });
      })
      .catch(function(error) {});

    if (sessionStorage.getItem("token") != null) {
      let name = [];
      name["name"] = sessionStorage.getItem("name");

      this.setState(
        {
          login: sessionStorage.getItem("token")
        },
        () => {
          this.props.addUser(name);
        }
      );
    }
  }

  signIn(event) {
    event.preventDefault();
    let self = this;

    if (this.state.login == null && sessionStorage.getItem("token") == null) {
      auth()
        .then(function(result) {
          // This gives you a GitHub Access Token. You can use it to access the GitHub API.
          let token = result.credential.accessToken;
          // The signed-in user info.
          let user = result.user;
          if (typeof Storage !== "undefined") {
            sessionStorage.setItem("token", token);
            sessionStorage.setItem("name", result.additionalUserInfo.username);
            let name = [];
            name["name"] = result.additionalUserInfo.username;
            self.setState(
              {
                login: token
              },
              () => {
                self.props.addUser(name);
              }
            );
          }
        })
        .catch(function(error) {
          // Handle Errors here.
          let errorCode = error.code;
          let errorMessage = error.message;
          // The email of the user's account used.
          let email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          let credential = error.credential;
          // ...
        });
    }
  }

  addComments(event, e) {
    event.preventDefault();
    let self = this;

    if (this.state.login == null && sessionStorage.getItem("token") == null) {
      auth()
        .then(function(result) {
          // This gives you a GitHub Access Token. You can use it to access the GitHub API.
          let token = result.credential.accessToken;
          // The signed-in user info.
          let user = result.user;

          if (typeof Storage !== "undefined") {
            sessionStorage.setItem("token", token);
            sessionStorage.setItem("name", result.additionalUserInfo.username);
            this.props.addUser({
              name: result.additionalUserInfo.username
            });
            let name = [];
            name["name"] = result.additionalUserInfo.username;
            self.setState(
              {
                login: token
              },
              () => {
                self.props.addUser(name);
              }
            );
          }
        })
        .catch(function(error) {
          // Handle Errors here.
          let errorCode = error.code;
          let errorMessage = error.message;
          // The email of the user's account used.
          let email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          let credential = error.credential;
          // ...
        });
    } else {
      let value;

      if (e == 1) {
        value = event.target.children[0].value;
      } else {
        value = event.target.value;
      }
      let self = this;
      let data = {};

      $.ajax({
        url:
          "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/" +
          this.props.issueId +
          "/comments?access_token=" +
          this.state.login,
        data: JSON.stringify({ body: value }),
        type: "POST",
        success: function() {
          self.getData();
        },
        error: function(xhr, status, error) {
          console.log(xhr.responseText);
        }
      });
    }
  }
  signout() {
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("name");
    this.props.addUser(null);
    this.setState({
      login: null
    });
  }

  render() {
    let comments;
    if (this.state.originalIssueState == 1) {
      let data = this.state.originalIssue;
      let icon = "";
      if (data["closed_by"] != null && data["closed_by"] != "") {
        icon = (
          <button className="issue-but">
            <svg
              class="octicon octicon-issue-closed closed"
              viewBox="0 0 16 16"
              version="1.1"
              width="16"
              height="16"
              aria-hidden="true"
            >
              <path
                fill-rule="evenodd"
                d="M7 10h2v2H7v-2zm2-6H7v5h2V4zm1.5 1.5l-1 1L12 9l4-4.5-1-1L12 7l-1.5-1.5zM8 13.7A5.71 5.71 0 0 1 2.3 8c0-3.14 2.56-5.7 5.7-5.7 1.83 0 3.45.88 4.5 2.2l.92-.92A6.947 6.947 0 0 0 8 1C4.14 1 1 4.14 1 8s3.14 7 7 7 7-3.14 7-7l-1.52 1.52c-.66 2.41-2.86 4.19-5.48 4.19v-.01z"
              />
            </svg>
            <span className="but-text">Closed</span>
          </button>
        );
      } else {
        icon = (
          <button className="issue-but">
            <svg
              class="octicon octicon-issue-opened open"
              viewBox="0 0 14 16"
              version="1.1"
              width="14"
              height="16"
              aria-hidden="true"
            >
              <path
                fill-rule="evenodd"
                d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7s-2.56 5.7-5.7 5.7A5.71 5.71 0 0 1 1.3 8c0-3.14 2.56-5.7 5.7-5.7zM7 1C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7-3.14-7-7-7zm1 3H6v5h2V4zm0 6H6v2h2v-2z"
              />
            </svg>
            <span className="but-text">Open</span>
          </button>
        );
      }

      if (this.state.originalCommmentsState == 1) {
        comments = this.state.originalCommments["data"].map((val, index) => {
          return (
            <div className="display-issue-card">
              <div className="display-issue-image display-issue-inline">
                <img
                  className="display-issue-img"
                  src={val.user["avatar_url"]}
                />
              </div>
              <div className="display-issue-inline display-issue-parent">
                <div className="display-issue-title">
                  <span className="author"> {val["user"]["login"]}</span>
                  &nbsp; commented &nbsp;
                  <span>{moment(val["created_at"]).fromNow()}</span>
                </div>
                <div className="display-issue-body">
                  <div
                    className="x"
                    onClick={() => {
                      this.deleteComment(val["id"]);
                    }}
                  >
                    {this.state.login == null ? (
                      ""
                    ) : (
                      <span className="x-span">X</span>
                    )}
                  </div>
                  <ReactMarkdown source={val["body"]} escapeHtml={false} />
                </div>
              </div>
            </div>
          );
        });
      }
      return (
        <div className="display-issue-component">
          <div className="display-issue-head">
            <IssueHead data={data.title} id={data.number} />
          </div>
          <div>
            {icon}&nbsp;
            <span className="issue-head">
              {data["user"]["login"]} opened this issue{" "}
              {moment(data.created_at).fromNow()}.{" "}
              {this.state.originalCommmentsState == 0
                ? 0
                : this.state.originalCommments.data.length}
              &nbsp; Comments
            </span>
            {this.state.login == null ? (
              <input
                type="button"
                value="Sign In "
                className="issue-but issue-but-sign"
                onClick={event => {
                  this.signIn(event);
                  // this.redirect();
                }}
              />
            ) : (
              <input
                type="button"
                value="Sign Out "
                className="issue-but issue-but-sign"
                onClick={() => {
                  this.signout();
                }}
              />
            )}
          </div>

          <div className="display-issue-list">
            <div className="display-issue-card">
              <div className="display-issue-image display-issue-inline">
                <img
                  className="display-issue-img"
                  src={data["user"]["avatar_url"]}
                />
              </div>
              <div className="display-issue-inline display-issue-parent">
                <div className="display-issue-title">
                  <span className="author"> {data["user"]["login"]}</span>{" "}
                  &nbsp; commented &nbsp;
                  <span> {moment(data.created_at).fromNow()}</span>
                </div>
                <div className="display-issue-body">
                  <ReactMarkdown source={data["body"]} escapeHtml={false} />
                </div>
              </div>
            </div>
            {this.state.originalCommmentsState == 1 ? comments : ""}
            {/* for input */}
            <div className="display-issue-card">
              <div className="display-issue-image display-issue-inline">
                {/* <img
                  className="display-issue-img"
                  src={data["user"]["avatar_url"]}
                /> */}
              </div>
              <div className="input-comment-div">
                <div className="display-issue-title ">
                  {/* <span className="author"> {data["user"]["login"]}</span>{" "}
                  &nbsp; commented &nbsp;
                  <span> {moment(data.created_at).fromNow()}</span> */}
                </div>
                {/* <div className="display-issue-body">{data["body"]}</div> */}
                <form
                  onSubmit={event => {
                    this.addComments(event, 1);
                  }}
                >
                  {this.state.login == null ? (
                    <div>
                      {" "}
                      <textarea
                        placeholder="PLEASE LOGIN FOR->   Leave a Comment... "
                        disabled
                        className="input-for-comment"
                        // onKeyPress={event => {
                        //   if (event.which == 13 || event.keyCode == 13) {
                        //     this.addComments(event, 2);
                        //     event.target.value = "";
                        //     return false;
                        //   }
                        //   return true;
                        // }}
                      />
                      <input
                        type="submit"
                        value="Sign In"
                        className="issue-but issue-but-sign"
                      />
                    </div>
                  ) : (
                    <div>
                      {" "}
                      <textarea
                        placeholder=" Leave a Comment... "
                        className="input-for-comment"
                        onKeyPress={event => {
                          if (event.which == 13 || event.keyCode == 13) {
                            this.addComments(event, 2);
                            event.target.value = "";
                            return false;
                          }
                          return true;
                        }}
                      />
                      <input
                        type="submit"
                        value="Comment"
                        className="issue-but"
                      />
                    </div>
                  )}
                </form>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return <div>Request is processing. Please wait....</div>;
  }
}
const mapStateToProps = state => {
  return { user: state.user };
};
export default connect(
  mapStateToProps,
  { addUser: addUser }
)(DisplayIssueComponent);
// export default DisplayIssueComponent;

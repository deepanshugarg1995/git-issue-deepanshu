export default (state = [], action) => {
  if (action.type == "FETCH_LABELS") {
    if (action.payload.status == 200) {
      return action.payload["data"];
    }
  }
  return state;
};

import { combineReducers } from "redux";
import issueList from "./issueList";
import filterList from "./filterList";
import labels from "./labels";
import addUser from "./addUser";

export default combineReducers({
  issues: issueList,
  filter: filterList,
  labels: labels,
  addUser: addUser
});

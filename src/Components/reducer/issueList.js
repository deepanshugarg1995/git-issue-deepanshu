export default (state = [], action) => {
  console.log(action, "reducer of issuelist");
  if (action.type == "FETCH_ISSUES") {
    return action.payload;
  }
  return state;
};

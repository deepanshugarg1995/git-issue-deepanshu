import React, { Component } from "react";
import { white, black } from "ansi-colors";
import moment from "moment";
import "./BodyComponent.css";
import Highlighter from "react-highlight-words";
import { BrowserRouter as Router, Link } from "react-router-dom";

class BodyIssue extends Component {
  render() {
    let colour = "";
    let style = { backgroundColor: "green" };
    // let date = this.props.data.created_at.replace(/[0-9](rd|th|st)/, "");

    return (
      <div className="bodyissue">
        <div className="issues">
          <div className="open-issue-image issue-inline">
            {this.props.data.state == "open" ? (
              <img
                src={require("../../public/open.PNG")}
                title="Still open"
                className="issue-open-img"
              />
            ) : (
              <img
                src={require("../../public/close.PNG")}
                title="Still open"
                className="issue-open-img"
              />
            )}
          </div>
          <div className="issue-inline comment-text">
            <span className="issue-head">
              {/* <a href="/issue/:isssueId">newewwwe</a> */}
              <Link
                to={`/issue/${this.props.data.number}`}
                style={{ color: "#292b2c", textDecoration: "none" }}
              >
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  searchWords={[this.props.text]}
                  autoEscape={true}
                  textToHighlight={this.props.data.title}
                />
              </Link>
            </span>
            <span>
              {this.props.data.labels.map((value, index) => {
                {
                  colour = "#" + value.color;
                  style.backgroundColor = colour;
                }
                return (
                  <div className="labels" style={{ ...style }}>
                    {}
                    <Highlighter
                      highlightClassName="YourHighlightClass"
                      searchWords={[this.props.text]}
                      autoEscape={true}
                      textToHighlight={value.name}
                    />
                  </div>
                );
              })}
            </span>
            <div className="extra-info">
              <span>
                #{this.props.data.number} opened hours{" "}
                {moment(this.props.data.created_at).fromNow()}
                &nbsp; by &nbsp;
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  searchWords={[this.props.text]}
                  autoEscape={true}
                  textToHighlight={this.props.data.user.login}
                />
              </span>
            </div>
          </div>

          <div className="comment-num">
            <img src={require("./comment.PNG")} />{" "}
            <span> {this.props.data.comments}</span>{" "}
          </div>
        </div>
      </div>
    );
  }
}

export default BodyIssue;

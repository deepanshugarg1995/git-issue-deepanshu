import React, { Component } from "react";
import Select from "react-select";
import "./BodyComponent.css";
// import { MultiSelectComponent } from "@syncfusion/ej2-react-dropdowns";
import MultiSelect from "@kenshooui/react-multi-select";
import { connect } from "react-redux";
import "@kenshooui/react-multi-select/dist/style.css";
// import "react-select/dist/react-select.css";
class BodyHead extends Component {
  constructor() {
    super();
    this.handleChange4 = this.handleChange4.bind(this);
  }
  state = {
    selectedOption: { value: null, label: "Author" },
    selectedOption2: { value: null, label: "State" },
    selectedOption3: { value: null, label: "Filter" },
    selectedOption4: {},
    multiValue: []
  };
  handleChange = selectedOption => {
    // debugger;
    this.setState({ selectedOption });
  };
  handleChange2 = selectedOption2 => {
    // debugger;
    this.setState({ selectedOption2 });
  };
  handleChange3 = selectedOption3 => {
    // debugger;
    this.setState({ selectedOption3 });
  };
  handleChange4 = option => {
    // debugger;
    // let data = this.state.selectedOption4;
    // data.push(selectedOption4);
    // this.setState({ selectedOption4 });

    this.setState(state => {
      return {
        multiValue: option
      };
    });
  };

  render() {
    let author = {};
    let authorarr = [];
    this.props.data.filter((value, index) => {
      if (!author[value["user"]["id"]]) {
        let id = value["user"]["id"];
        let name = value["user"]["login"];
        // author.push({ id: { id }, name: { name } });
        author[id] = name;
        authorarr.push({ value: id, label: name });
      }
    });
    const sort = [
      { value: 0, label: "New" },
      { value: 1, label: "Old" },
      { value: 2, label: "Lowest Comment" },
      { value: 3, label: "Highest Comment" }
    ];
    const state = [{ value: 0, label: "closed" }, { value: 1, label: "open" }];

    let authorfilter = "";
    let labels = [];
    let trialLabels = [];

    if (this.props.labels != [] && this.props.labels != undefined) {
      this.props.labels.map(val => {
        let id = val.id;
        let label = val.name;
        labels.push({ value: id, label: label });
        trialLabels.push(label);
      });
    }

    return (
      <div className="bodyhead">
        <div className="issueopen">Current Issues {this.props.num}</div>
        <div className="operations">
          <div className="filter cursor">
            <Select
              value={this.state.selectedOption}
              onChange={selectedOption => {
                this.handleChange(selectedOption);
                if (selectedOption["value"] != null) {
                  this.props.filter(selectedOption);
                }
              }}
              options={authorarr}
            />
          </div>
          <div className="sort state cursor">
            <Select
              value={this.state.selectedOption2}
              onChange={selectedOption2 => {
                this.handleChange2(selectedOption2);
                if (selectedOption2["value"] != null) {
                  this.props.changeState(selectedOption2["label"]);
                }
              }}
              name="deep"
              options={state}
            />
          </div>
          <div className="sort cursor">
            <Select
              value={this.state.selectedOption3}
              onChange={selectedOption3 => {
                this.handleChange3(selectedOption3);
                if (selectedOption3["value"] != null) {
                  this.props.func(selectedOption3["value"]);
                }
              }}
              options={sort}
            />
          </div>
          <div className="sort-by-label cursor">
            {/* <MultiSelectComponent id="defaultelement" dataSource={this.labels} mode="Default" fields={this.fields} placeholder="Favorite Sports" /> */}
            {labels == [] ? (
              ""
            ) : (
              <Select
                value={this.state.multiValue}
                onChange={selectedOption4 => {
                  this.handleChange4(selectedOption4);

                  // if (selectedOption3["value"] != null) {
                  //   this.props.func(selectedOption3["value"]);
                  // }
                  if (selectedOption4 != []) {
                    this.props.label(selectedOption4);
                  }
                }}
                placeholder="Labels"
                options={labels}
                isMulti={true}
              />
            )}
          </div>
        </div>
        <div />
      </div>
    );
  }
}

// export default BodyHead;

const mapStateToProps = state => {
  return { labels: state.labels };
};
export default connect(
  mapStateToProps,
  null
)(BodyHead);

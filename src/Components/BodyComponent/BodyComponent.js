import React from "react";
import ReactDOM from "react-dom";
import moment from "moment";

import BodyIssue from "./BodyIssue";
import BodyHead from "./Bodyhead";
import SearchBar from "./SearchBar";
import "./BodyComponent.css";
import { BrowserRouter as Router, NavLink } from "react-router-dom";
import issue from "../../public/issues.json";
import auth from "../../auth";

class BodyComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      currentType: "",
      originalList: "",
      issuelist: "",
      issuePerPage: "",
      text: ""
    };
  }

  // sorting(type) {}
  sortType(value) {
    if (value == "0") {
      this.byNew();
    } else if (value == "1") {
      this.byOld();
    } else if (value == "2") {
      this.byLow();
    } else {
      this.byHigh();
    }
  }
  byNew() {
    let data = this.state.issuelist;
    let bydate = data.slice();
    bydate.sort(function(a, b) {
      let aa = +new Date(a["created_at"]);
      let bb = +new Date(b["created_at"]);
      return bb - aa;
    });

    this.setState({
      issuelist: bydate
    });
  }
  byOld() {
    let data = this.state.issuelist;
    let bydate = data.slice();
    bydate.sort(function(a, b) {
      let aa = +new Date(a["created_at"]);
      let bb = +new Date(b["created_at"]);
      return aa - bb;
    });
    this.setState({
      issuelist: bydate
    });
  }
  byLow() {
    let data = this.state.issuelist;
    let bydate = data.slice();
    bydate.sort(function(a, b) {
      let aa = +new Date(a["comments"]);
      let bb = +new Date(b["comments"]);
      return aa - bb;
    });
    this.setState({
      issuelist: bydate
    });
  }
  byHigh() {
    let data = this.state.issuelist;
    let bydate = data.slice();
    bydate.sort(function(a, b) {
      let aa = +new Date(a["comments"]);
      let bb = +new Date(b["comments"]);
      return bb - aa;
    });
    this.setState({
      issuelist: bydate
    });
  }

  filterByName(obj) {
    let newFilter = this.state.originalList.filter((value, index) => {
      if (value["user"]["id"] == obj["value"]) {
        return value;
      }
    });
    this.setState({
      issuelist: newFilter
    });
  }

  searching(text) {
    let search = this.state.originalList.filter((value, index) => {
      let re = new RegExp(text, "g", "i");
      // "pattern matching .".replace(re, "regex");
      if (
        value["title"].search(re) != -1 ||
        value["user"]["login"].search(re) != -1 ||
        value["body"].search(re) != -1
      ) {
        return value;
      }
    });
    this.setState({
      text: text,
      issuelist: search
    });
  }
  changeState(stateval) {
    let state = this.state.issuelist.filter((value, index) => {
      if (stateval == value["state"]) {
        return value;
      }
    });
    this.setState({
      issuelist: state
    });
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props;
    if (oldProps.data != newProps.data) {
      this.setState({
        originalList: this.props.data,
        issuelist: this.props.data
      });
      if (this.props.data != this.state.originalList) {
      }
    }
  }
  componentDidMount() {
    this.setState({
      originalList: this.props.data,
      issuelist: this.props.data
    });
  }

  render() {
    if (this.state.originalList == "") {
      return <div>Processing Please Wait.....!!!!</div>;
    } else {
      return (
        <div className="bodycomponent-parent">
          <SearchBar
            search={i => {
              this.searching(i);
            }}
          />
          {/* <button onClick={()=>{this.props.addIssue}}></button> */}

          <div className="bodycomponent">
            <BodyHead
              num={
                this.state.issuePerPage == ""
                  ? this.state.issuelist.length
                  : this.state.issuePerPage.length
              }
              // func={i => {
              //   this.sortType(i);
              // }}
              // func={this.props.sort}
              // filter={id => {
              //   this.filterByName(id);
              // }}
              // originaldata={}
              data={this.state.originalList}
              // changeState={i => {
              //   this.changeState(i);
              // }}
              func={this.props.sort}
              changeState={this.props.state}
              filter={this.props.author}
              label={this.props.label}
            />
            {this.state.issuelist.map((value, index) => {
              return <BodyIssue data={value} text={this.state.text} />;
            })}
          </div>
        </div>
      );
    }
  }
}
export default BodyComponent;

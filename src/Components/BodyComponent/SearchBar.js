import React from "react";
import "./BodyComponent.css";
function SearchBar(props) {
  return (
    <div className="search-bar">
      <input
        type="text"
        className="search-bar-input"
        placeholder="Search for anything"
        onKeyPress={event => {
          if (event.which == 13 || event.keyCode == 13) {
            console.log(event.target.value);
            props.search(event.target.value);
            return false;
          }
          return true;
        }}
      />
    </div>
  );
}

export default SearchBar;

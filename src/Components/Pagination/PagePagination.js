import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes, { func } from "prop-types";
import ReactPaginate from "react-paginate";
import $ from "jquery";
import "./Pagination.css";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import BodyComponent from "../BodyComponent/BodyComponent";
import axios from "axios";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { connect } from "react-redux";
import { fetchissue } from "../action/index";
import { filterList } from "../action/filterList";
import { labels } from "../action/labels";

window.React = React;

export class CommentList extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  };

  render() {
    let commentNodes = this.props.data.map(function(comment, index) {
      return <div key={index}>{comment.comment}</div>;
    });

    return (
      <div id="project-comments" className="commentList">
        <ul>{commentNodes}</ul>
      </div>
    );
  }
}

class PagePagination extends Component {
  // static propTypes = {
  //   url: PropTypes.string.isRequired,
  //   author: PropTypes.string.isRequired,
  //   // perPage: PropTypes.number.isRequired
  // };

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      nodata: "",
      offset: 0,
      page: 1,
      url:
        "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=",
      filterByAuthor: "",
      sortByState: "",
      sortByTime: "",
      labels: []
    };
  }

  loadCommentsFromServer() {
    this.setState({
      page: this.props.data.match.params["pageNo"]
    });
    $.ajax({
      url: this.state.url + this.props.data.match.params["pageNo"],

      type: "GET",
      crossDomain: true,
      success: data => {
        this.setState({
          data: data
        });
      },

      error: (xhr, status, err) => {}
    });
  }

  fetching() {
    let data = {};
    if (this.state.sortByTime != "") {
      if (this.state.sortByTime == "new") {
        data["sort"] = "created";
        data["direction"] = "desc";
      }
      if (this.state.sortByTime == "old") {
        data["sort"] = "created";
        data["direction"] = "asc";
      }
      if (this.state.sortByTime == "low") {
        data["sort"] = "comments";
        data["direction"] = "asc";
      }
      if (this.state.sortByTime == "high") {
        data["sort"] = "comments";
        data["direction"] = "desc";
      }
    }
    if (this.state.filterByAuthor != "") {
      data["creator"] = this.state.filterByAuthor;
    }
    if (this.state.sortByState) {
      data["state"] = this.state.sortByState;
    }
    if (this.state.labels != []) {
      let label = "";
      this.state.labels.map(val => {
        label += val + ",";
      });
      label = label.substring(0, label.length - 1);

      data["labels"] = label;
    }

    this.props.fetchissue(data, this.props.data.match.params.pageNo);
    // $.ajax({
    //   url: this.state.url + this.props.data.match.params["pageNo"],
    //   data: data,
    //   dataType: "json",
    //   type: "GET",

    //   success: response => {
    //     if (data != []) {
    //       this.setState({
    //         data: response
    //       });
    //     } else {
    //       data = [];
    //     }
    //   },

    //   error: (xhr, status, err) => {}
    // });
  }

  sortType(value) {
    if (value == "0") {
      this.byNew();
    } else if (value == "1") {
      this.byOld();
    } else if (value == "2") {
      this.byLow();
    } else {
      this.byHigh();
    }
  }

  filterByAuthor(obj) {
    this.setState(
      {
        filterByAuthor: obj["label"]
      },
      () => {
        this.fetching();
      }
    );
  }

  byNew() {
    this.setState({ sortByTime: "new" }, () => {
      this.fetching();
    });

    // this.fetching();
  }

  byOld() {
    this.setState(
      {
        sortByTime: "old"
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }
  byLow() {
    this.setState(
      {
        sortByTime: "low"
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }
  byHigh() {
    this.setState(
      {
        sortByTime: "high"
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }
  filterByLabels(arr) {
    let response = [];
    arr.map(val => {
      response.push(val["label"]);
    });
    response.join(",");

    this.setState(
      {
        labels: response
      },
      () => {
        this.fetching();
      }
    );
  }

  filterByState(state) {
    this.setState(
      {
        sortByState: state
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }

  componentDidMount() {
    this.setState(
      {
        page: this.props.data.match.params["pageNo"]
      },
      () => {
        this.fetching();
        this.props.labels();
      }
    );
  }
  removeFilter() {
    this.props.filterList(null);
    this.props.fetchissue();
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props;
    if (oldProps.data != newProps.data) {
      this.fetching();

      // this.props.fetchissue(this.props.data.match.params["pageNo"]);

      //   this.setState(
      //     {
      //       page: this.props.data.match.params["pageNo"]
      //     },
      //     () => {
      //       this.loadCommentsFromServer();
      //     }
      //   );
    }
  }

  handlePageClick = data => {
    let page = parseInt(data["selected"]) + 1;
    this.props.data.history.push(`/page/${page}`);
  };

  render() {
    console.log(this.props);
    if (
      this.props.issues != [] &&
      this.props.issues != undefined &&
      this.props.issues != "no data"
    ) {
      return (
        <React.Fragment>
          <HeaderComponent data={this.props.issues} />
          <BodyComponent
            data={
              // this.props.filter == null ? this.props.issues : this.props.filter
              this.props.issues
            }
            author={i => {
              this.filterByAuthor(i);
            }}
            state={i => {
              this.filterByState(i);
            }}
            sort={i => {
              this.sortType(i);
            }}
            label={i => {
              this.filterByLabels(i);
            }}
          />
          <div className="commentBox">
            {/* <CommentList data={this.state.data} /> */}

            <ReactPaginate
              previousLabel={"previous"}
              nextLabel={"next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              forcePage={parseInt(this.props.data.match.params["pageNo"]) - 1}
              pageCount={this.state.pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={3}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
              activeLinkClassName={"activeAlink"}
            />
          </div>
        </React.Fragment>
      );
    } else if (this.props.issues == "no data") {
      return (
        <div>
          <HeaderComponent />
          <div className="no-data-div">
            <img src={require("../../public/nodata.gif")} />
            <button
              onClick={() => {
                this.removeFilter();
              }}
            >
              {" "}
              Remove Filter
            </button>
          </div>
        </div>
      );
    } else {
      return <div>Data is Loading.....</div>;
    }
  }
}

const mapStateToProps = state => {
  return { issues: state.issues, filter: state.filter, labels: state.labels };
};
export default connect(
  mapStateToProps,
  { fetchissue: fetchissue, filterList: filterList, labels: labels }
)(PagePagination);
// export default PagePagination;

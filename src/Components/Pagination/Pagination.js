import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes, { func } from "prop-types";
import ReactPaginate from "react-paginate";
import $ from "jquery";
import "./Pagination.css";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import BodyComponent from "../BodyComponent/BodyComponent";
import axios from "axios";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { fetchissue } from "../action/index";
import { filterList } from "../action/filterList";

window.React = React;

export class CommentList extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired
  };

  render() {
    let commentNodes = this.props.data.map(function(comment, index) {
      return <div key={index}>{comment.comment}</div>;
    });

    return (
      <div id="project-comments" className="commentList">
        <ul>{commentNodes}</ul>
      </div>
    );
  }
}

class Pagination extends Component {
  // static propTypes = {
  //   url: PropTypes.string.isRequired,
  //   author: PropTypes.string.isRequired,
  //   // perPage: PropTypes.number.isRequired
  // };

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      nodata: "",
      offset: 0,
      page: 1,
      url:
        "https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=",
      filterByAuthor: "",
      sortByState: "",
      sortByTime: ""
    };
  }

  loadCommentsFromServer() {
    $.ajax({
      url: this.state.url + this.state.page,
      // url:
      //   "https://github.com/freeCodeCamp/freeCodeCamp/issues/show_menu_content?partial=issues%2Ffilters%2Fauthors_content&q=is%3Aissue+is%3Aopen",

      // dataType: "json",
      type: "GET",
      crossDomain: true,
      success: data => {
        this.setState({
          data: data
        });
      },

      error: (xhr, status, err) => {}
    });
  }

  fetching() {
    let data = {};
    if (this.state.sortByTime != "") {
      if (this.state.sortByTime == "new") {
        data["sort"] = "created";
        data["direction"] = "desc";
      }
      if (this.state.sortByTime == "old") {
        data["sort"] = "created";
        data["direction"] = "asc";
      }
      if (this.state.sortByTime == "low") {
        data["sort"] = "comments";
        data["direction"] = "asc";
      }
      if (this.state.sortByTime == "high") {
        data["sort"] = "comments";
        data["direction"] = "desc";
      }
    }
    if (this.state.filterByAuthor != "") {
      data["creator"] = this.state.filterByAuthor;
    }
    if (this.state.sortByState) {
      data["state"] = this.state.sortByState;
    }
    // $.ajax({
    //   url: this.state.url + this.state.page,
    //   data: data,
    //   dataType: "json",
    //   type: "GET",

    //   success: response => {
    //     // this.setState({
    //     //   data: data.comments,
    //     //   pageCount: Math.ceil(data.meta.total_count / data.meta.limit)
    //     // });
    //     if (data != []) {
    //       this.setState({
    //         data: response
    //       });
    //     } else {
    //       data = [];
    //     }
    //   },

    //   error: (xhr, status, err) => {}
    // });
    console.log("insinde ", data);
    this.props.fetchissue(data, 1);
    this.props.filterList(data);
  }

  sortType(value) {
    if (value == "0") {
      this.byNew();
    } else if (value == "1") {
      this.byOld();
    } else if (value == "2") {
      this.byLow();
    } else {
      this.byHigh();
    }
  }

  filterByAuthor(obj) {
    this.setState(
      {
        filterByAuthor: obj["label"]
      },
      () => {
        this.fetching();
      }
    );

    // this.fetching();
  }

  byNew() {
    this.setState({ sortByTime: "new" }, () => {
      this.fetching();
    });

    // this.fetching();
  }

  byOld() {
    this.setState(
      {
        sortByTime: "old"
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }
  byLow() {
    this.setState(
      {
        sortByTime: "low"
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }
  byHigh() {
    this.setState(
      {
        sortByTime: "high"
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }

  filterByState(state) {
    this.setState(
      {
        sortByState: state
      },
      () => {
        this.fetching();
      }
    );
    // this.fetching();
  }

  loadAuthors() {
    let xhr = new XMLHttpRequest();
    let response = xhr.open(
      "GET",
      "https://github.com/freeCodeCamp/freeCodeCamp/issues/show_menu_content?partial=issues%2Ffilters%2Fauthors_content&q=is%3Aissue+is%3Aopen",
      true
    );
  }

  componentDidMount() {
    // $.ajax({
    //   url: this.state.url + this.state.page,

    //   type: "GET",

    //   success: data => {
    //     this.setState({
    //       data: data
    //     });
    //   },

    //   error: (xhr, status, err) => {
    //     console.error(status, err.toString()); // eslint-disable-line
    //   }
    // });
    // this.props.fetching([],1);
    this.fetching();
  }

  handlePageClick = data => {
    let page = parseInt(data["selected"]) + 1;
    this.props.data.history.push(`/page/${page}`);

    // this.setState(
    //   {
    //     page: page + 1
    //   },
    //   () => {
    //     this.loadCommentsFromServer();
    //   }
    // );
  };

  render() {
    if (this.props.issues != []) {
      return (
        <React.Fragment>
          <HeaderComponent data={this.props.issues} />

          <BodyComponent
            data={this.props.issues}
            author={i => {
              this.filterByAuthor(i);
            }}
            state={i => {
              this.filterByState(i);
            }}
            sort={i => {
              this.sortType(i);
            }}
          />
          <div className="commentBox">
            {/* <CommentList data={this.state.data} /> */}

            <ReactPaginate
              previousLabel={"previous"}
              nextLabel={"next"}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={this.state.pageCount}
              marginPagesDisplayed={2}
              pageRangeDisplayed={3}
              onPageChange={this.handlePageClick}
              containerClassName={"pagination"}
              subContainerClassName={"pages pagination"}
              activeClassName={"active"}
              activeLinkClassName={"activeAlink"}
            />
          </div>
        </React.Fragment>
      );
    } else {
      return <div>Data is Loading.....</div>;
    }
  }
}

// export default Pagination;

const mapStateToProps = state => {
  console.log(state, " maps state to props");
  return { issues: state.issues, filter: state.filter };
};
export default connect(
  mapStateToProps,
  { fetchissue: fetchissue, filterList: filterList }
)(Pagination);

import issueList from "../apis/issueList";
export const labels = (data = [], pageNo = 1) => {
  return async (dispatch, getState) => {
    const response = await issueList.get("/labels?per_page=100");

    dispatch({
      type: "FETCH_LABELS",
      payload: response
    });
  };
};

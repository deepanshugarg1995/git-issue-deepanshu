import issueList from "../apis/issueList";
export const fetchissue = (data = [], pageNo = 1) => {
  return async (dispatch, getState) => {
    console.log();
    const response = await issueList.get("issues?page=" + pageNo, {
      params: data
    });
    console.log(response);
    console.log(response, " inside action of filter list", response.data == []);
    if (response.status == 200) {
      if (response.data == [] || response.data.length == 0) {
        dispatch({
          type: "FETCH_ISSUES",
          payload: "no data"
        });
      } else {
        dispatch({
          type: "FETCH_ISSUES",
          payload: response.data
        });
      }
    } else {
      dispatch({
        type: "FETCH_ISSUES",
        payload: "no data"
      });
    }
  };
};

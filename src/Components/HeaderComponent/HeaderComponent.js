import React from "react";
import ReactDom from "react-dom";
import "./HeaderComponent.css";
import { connect } from "react-redux";
class HeaderComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      user: null
    };
  }
  componentDidMount() {
    if (this.props.user != undefined && this.props.user != null) {
      if (this.props.user[0] != null) {
        this.setState({ user: this.props.user.name });
      }
    }
  }
  componentDidUpdate(props) {
    if (this.props != props) {
      if (this.props.user != null) {
        if (this.props.user[0] != null) {
          this.setState({ user: this.props.user[0].name });
        } else {
          this.setState({ user: null });
        }
      }
    }
  }

  render() {
    {
    }
    return (
      <div className="HeaderComponent">
        <div className="header-child-one">
          <input className="header-input" placeholder="search" />
          <span className="user-name" title="Login">
            {/* {this.props.user!=null ?  this.props.user != undefined ? this.props.user : "" :""} */}
            {this.state.user != null ? this.state.user : "User"}
          </span>
        </div>
        <div className="header-child-two">
          <div className="freecodecamp  child-two">freecodecamp</div>
          <div className="child-two"> &nbsp;/&nbsp; </div>
          <div className="freecodecamp child-two"> FreeCodeCamp</div>
        </div>
        <div className="header-child-three">
          <button className="issuenum">Issues number</button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { user: state.addUser };
};
export default connect(
  mapStateToProps,
  null
)(HeaderComponent);
// export default HeaderComponent;
